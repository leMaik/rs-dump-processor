/*
 * Copyright (c) 2016 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of the RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.renderer.rendering;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.wertarbyte.renderservice.libchunky.ChunkyRenderDump;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.zip.GZIPInputStream;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class ApiClient {

  private static final Gson gson = new Gson();
  private final String baseUrl;
  private final OkHttpClient client;

  public ApiClient(String baseUrl, String apiKey) {
    this.baseUrl = baseUrl;
    client = new OkHttpClient.Builder()
        .readTimeout(30, TimeUnit.MINUTES)
        .writeTimeout(30, TimeUnit.MINUTES)
        .addInterceptor(chain -> chain.proceed(
            chain.request().newBuilder()
                .header("X-Api-Key", apiKey)
                .build()))
        .retryOnConnectionFailure(true)
        .build();
  }

  public CompletableFuture<RenderServiceInfo> getInfo() {
    CompletableFuture<RenderServiceInfo> result = new CompletableFuture<>();

    client.newCall(new Request.Builder()
        .url(baseUrl + "/info").get().build())
        .enqueue(new Callback() {
          @Override
          public void onFailure(Call call, IOException e) {
            result.completeExceptionally(e);
          }

          @Override
          public void onResponse(Call call, Response response) {
            try {
              if (response.code() == 200) {
                try (
                    ResponseBody body = response.body();
                    InputStreamReader reader = new InputStreamReader(body.byteStream())
                ) {
                  result.complete(gson.fromJson(reader, RenderServiceInfo.class));
                } catch (IOException e) {
                  result.completeExceptionally(e);
                }
              } else {
                result.completeExceptionally(
                    new IOException("The render service info could not be downloaded"));
              }
            } catch (Exception e) {
              result.completeExceptionally(e);
            } finally {
              response.close();
            }
          }
        });

    return result;
  }

  public CompletableFuture<Job> getJob(String jobId) {
    CompletableFuture<Job> result = new CompletableFuture<>();

    client.newCall(new Request.Builder()
        .url(baseUrl + "/jobs/" + jobId).get().build())
        .enqueue(new Callback() {
          @Override
          public void onFailure(Call call, IOException e) {
            result.completeExceptionally(e);
          }

          @Override
          public void onResponse(Call call, Response response) {
            try {
              if (response.code() == 200) {
                try (
                    ResponseBody body = response.body();
                    InputStreamReader reader = new InputStreamReader(body.byteStream())
                ) {
                  result.complete(gson.fromJson(reader, Job.class));
                } catch (IOException e) {
                  result.completeExceptionally(e);
                }
              } else {
                if (response.code() == 404 && response.body().string()
                    .contains("Job not found")) {
                  result.complete(null);
                } else {
                  result
                      .completeExceptionally(new IOException("The job could not be downloaded"));
                }
              }
            } catch (Exception e) {
              result.completeExceptionally(e);
            } finally {
              response.close();
            }
          }
        });

    return result;
  }

  public CompletableFuture<JsonObject> getScene(Job job) {
    CompletableFuture<JsonObject> result = new CompletableFuture<>();

    client.newCall(new Request.Builder()
        .url(baseUrl + job.getSceneUrl()).get().build())
        .enqueue(new Callback() {
          @Override
          public void onFailure(Call call, IOException e) {
            result.completeExceptionally(e);
          }

          @Override
          public void onResponse(Call call, Response response) {
            try {
              if (response.code() == 200) {
                try (
                    ResponseBody body = response.body();
                    InputStreamReader reader = new InputStreamReader(body.byteStream())
                ) {
                  result.complete(gson.fromJson(reader, JsonObject.class));
                } catch (IOException e) {
                  result.completeExceptionally(e);
                }
              } else {
                result.completeExceptionally(new IOException("The scene could not be downloaded"));
              }
            } catch (Exception e) {
              result.completeExceptionally(e);
            } finally {
              response.close();
            }
          }
        });

    return result;
  }

  public CompletableFuture<ChunkyRenderDump> getLatestDump(String jobId) {
    CompletableFuture<ChunkyRenderDump> result = new CompletableFuture<>();

    client.newCall(new Request.Builder()
        .url(baseUrl + "/jobs/" + jobId + "/latest.dump")
        .get()
        .build())
        .enqueue(new Callback() {
          @Override
          public void onFailure(Call call, IOException e) {
            result.completeExceptionally(e);
          }

          @Override
          public void onResponse(Call call, Response response) throws IOException {
            try {
              if (response.code() == 200) {
                result.complete(ChunkyRenderDump.fromStream(
                    new DataInputStream(new GZIPInputStream(response.body().byteStream()))));
              } else if (response.isSuccessful()) {
                result.complete(null);
              } else {
                result.completeExceptionally(new IOException("Request did not return status 200"));
              }
            } catch (Exception e) {
              result.completeExceptionally(e);
            } finally {
              response.close();
            }
          }
        });

    return result;
  }

  public Future<ChunkyRenderDump> getDump(String dumpFileId) {
    CompletableFuture<ChunkyRenderDump> result = new CompletableFuture<>();

    client.newCall(new Request.Builder()
        .url(baseUrl + "/files/" + dumpFileId)
        .get()
        .build())
        .enqueue(new Callback() {
          @Override
          public void onFailure(Call call, IOException e) {
            result.completeExceptionally(e);
          }

          @Override
          public void onResponse(Call call, Response response) {
            try {
              if (response.code() == 200) {
                result.complete(ChunkyRenderDump.fromStream(
                    new DataInputStream(new GZIPInputStream(response.body().byteStream()))));
              } else if (response.body().string()
                  .contains("{\"statusCode\":404,\"message\":\"Not Found\"}")) {
                result.complete(null);
              } else {
                result.completeExceptionally(new IOException("Request did not return status 200"));
              }
            } catch (Exception e) {
              result.completeExceptionally(e);
            } finally {
              response.close();
            }
          }
        });

    return result;
  }

  public CompletableFuture<Response> sendDump(String jobId, byte[] dump, byte[] pngImage,
      String mergedDumpFileId) {
    CompletableFuture<Response> result = new CompletableFuture<>();

    MultipartBody.Builder body = new MultipartBody.Builder()
        .setType(MediaType.parse("multipart/form-data"))
        .addFormDataPart("dump", "scene.dump",
            RequestBody.create(MediaType.parse("application/octet-stream"), dump))
        .addFormDataPart("picture", "scene.png",
            RequestBody.create(MediaType.parse("image/png"), pngImage));

    if (mergedDumpFileId != null) {
      body = body.addFormDataPart("mergedDumpFile", mergedDumpFileId);
    }

    client.newCall(new Request.Builder()
        .url(baseUrl + "/jobs/" + jobId + "/latestDump")
        .post(body.build())
        .build()
    )
        .enqueue(new Callback() {
          @Override
          public void onFailure(Call call, IOException e) {
            result.completeExceptionally(e);
          }

          @Override
          public void onResponse(Call call, Response response) throws IOException {
            result.complete(response);
          }
        });

    return result;
  }
}
