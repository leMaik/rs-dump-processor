/*
 * Copyright (c) 2016 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of the RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.renderer.rendering;

import com.google.gson.JsonObject;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.QueueingConsumer;
import com.rabbitmq.client.QueueingConsumer.Delivery;
import com.wertarbyte.renderservice.libchunky.ChunkyRenderDump;
import com.wertarbyte.renderservice.libchunky.ChunkyRenderDump.Postprocess;
import com.wertarbyte.renderservice.util.Tuple;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
import javax.imageio.ImageIO;
import okhttp3.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DumpProcessorWorker implements Runnable {

  private static final Logger LOGGER = LogManager.getLogger(DumpProcessorWorker.class);

  private final Delivery delivery;
  private final Channel channel;
  private final ApiClient apiClient;
  private Tuple<String, ChunkyRenderDump> previous;

  public DumpProcessorWorker(QueueingConsumer.Delivery delivery, Channel channel,
      ApiClient apiClient) {
    this.delivery = delivery;
    this.channel = channel;
    this.apiClient = apiClient;
  }

  @Override
  public void run() {
    try {
      String jobId;
      String dumpFileId = null;
      final ChunkyRenderDump dump;
      final Job job;
      try (DataInputStream in = new DataInputStream(new ByteArrayInputStream(delivery.getBody()))) {
        jobId = in.readUTF();
        if (jobId.equals("v2")) {
          jobId = in.readUTF();
          dumpFileId = in.readUTF();
          job = apiClient.getJob(jobId).get(10, TimeUnit.MINUTES);
          if (job == null) {
            LOGGER.info("Job " + jobId + " was deleted, skipping and removing it from the queue");
            channel.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
            return;
          }
          if (job.isCancelled()) {
            LOGGER.info("Job " + jobId + " is cancelled, skipping and removing it from the queue");
            channel.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
            return;
          }
          LOGGER.info("Downloading dump to be merged into " + jobId + " (" + dumpFileId + ")...");
          dump = apiClient.getDump(dumpFileId).get(30, TimeUnit.MINUTES);
          if (dump == null) {
            LOGGER.info("Dump file " + dumpFileId
                + " does not exist, skipping and removing it from the queue");
            channel.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
            return;
          }
        } else {
          dump = ChunkyRenderDump.fromStream(new DataInputStream(new GZIPInputStream(in)));
          job = apiClient.getJob(jobId).get(10, TimeUnit.MINUTES);
          if (job == null) {
            LOGGER.info("Job " + jobId + " was deleted, skipping and removing it from the queue");
            channel.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
            return;
          }
          if (job.isCancelled()) {
            LOGGER.info("Job " + jobId + " is cancelled, skipping and removing it from the queue");
            channel.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
            return;
          }
        }
      }

      LOGGER.info("Merging a dump for " + jobId + " (+ " + dump.getSpp() + " SPP)");

      LOGGER.info("Downloading scene files...");
      CompletableFuture<ChunkyRenderDump> previousDump;
      if (previous != null && previous.getFirst().equals(job.getId())
          && previous.getSecond().getSpp() == job.getSpp()) {
        LOGGER.info(("Re-using previous dump"));
        previousDump = CompletableFuture.completedFuture(previous.getSecond());
        previous = null;
      } else {
        previousDump = apiClient.getLatestDump(jobId);
      }
      CompletableFuture<JsonObject> sceneDescription = apiClient.getScene(job);

      if (previousDump.get() != null) { // there was a dump before
        int expectedSpp = previousDump.get().getSpp() + dump.getSpp();
        LOGGER.info("Merging...");
        dump.mergeDump(previousDump.get());

        if (dump.getSpp() != expectedSpp) {
          LOGGER.warn("Expected " + expectedSpp + " SPP after merge, but got " + dump.getSpp()
              + ". Dump is removed.");
          // TODO the dump needs to be re-rendered...
          try {
            channel.basicNack(delivery.getEnvelope().getDeliveryTag(), false, false);
          } catch (IOException e) {
            LOGGER.error("Could not nack a failed task", e);
          }
          return;
        }
      }
      LOGGER.info("Dump now has " + dump.getSpp() + " SPP");

      Future<byte[]> dumpFile = CompletableFuture.supplyAsync(() -> {
        try (ByteArrayOutputStream bos = new ByteArrayOutputStream()) {
          // NOTE: GZIPOutputStream must be explicitly closed to finish writing (flushing will not help)
          try (DataOutputStream out = new DataOutputStream(new GZIPOutputStream(bos))) {
            dump.writeDump(out);
          }
          return bos.toByteArray();
        } catch (IOException e) {
          throw new CompletionException(e);
        }
      });
      Future<byte[]> pngFile = sceneDescription.thenApply((sd) -> {
        try (ByteArrayOutputStream bos = new ByteArrayOutputStream()) {
          BufferedImage image = dump.getPicture(
              sd.get("exposure").getAsDouble(),
              Postprocess.valueOfOrDefault(sd.get("postprocess").getAsString(), Postprocess.GAMMA)
          );
          ImageIO.write(image, "png", bos);
          return bos.toByteArray();
        } catch (IOException e) {
          throw new CompletionException(e);
        }
      });

      LOGGER.info("Uploading...");
      try (Response response = apiClient
          .sendDump(jobId, dumpFile.get(30, TimeUnit.MINUTES), pngFile.get(), dumpFileId)
          .get(30, TimeUnit.MINUTES)) {
        if (response.isSuccessful()) {
          if (dump.getSpp() < job.getTargetSpp()) {
            previous = new Tuple<>(job.getId(), dump);
          }
          LOGGER.info("Done");
          channel.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
        } else {
          LOGGER.info("Upload failed");
          channel.basicNack(delivery.getEnvelope().getDeliveryTag(), false, true);
        }
      }
    } catch (Exception e) {
      LOGGER.warn("An error occurred while processing a task", e);
      if (channel.isOpen()) {
        try {
          channel.basicNack(delivery.getEnvelope().getDeliveryTag(), false, true);
        } catch (IOException e1) {
          LOGGER.error("Could not nack a failed task", e);
        }
      }
    }
  }
}
