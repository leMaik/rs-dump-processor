/*
 * Copyright (c) 2016 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of the RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.renderer.rendering;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.QueueingConsumer;
import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeoutException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * A renderer worker thread.
 */
public class DumpProcessor extends Thread {

  private static final Logger LOGGER = LogManager.getLogger(DumpProcessor.class);
  private final int MAX_RESTART_DELAY_SECONDS = 15 * 60; // 15 minutes
  private final String masterServerUrl;
  private ConnectionFactory factory;
  private Connection conn;
  private Channel channel;
  private int nextRestartDelaySeconds = 1;
  private final ExecutorService executorService;

  public DumpProcessor(String masterServerUrl) {
    this.masterServerUrl = masterServerUrl;
    executorService = Executors.newFixedThreadPool(1);
  }

  @Override
  public void run() {
    ApiClient apiClient = new ApiClient(masterServerUrl, System.getenv("API_KEY"));
    try {
      factory = new ConnectionFactory();
      factory.setUri(System.getenv("RABBITMQ_URI"));
    } catch (Exception e) {
      LOGGER.error("Initialization failed", e);
      return;
    }

    while (!interrupted()) {
      LOGGER.info("Connecting");
      try {
        connect();
        LOGGER.info("Connected");
        nextRestartDelaySeconds = 1;

        QueueingConsumer consumer = new QueueingConsumer(channel);
        channel.basicQos(1); // prefetch one job to make dump download faster
        channel.basicConsume("rs_dumps", false, consumer);

        while (!interrupted() && channel.isOpen()) {
          try {
            executorService
                .submit(new DumpProcessorWorker(consumer.nextDelivery(), channel, apiClient));
          } catch (InterruptedException e) {
            LOGGER.info("Worker loop interrupted", e);
            break;
          }
        }
      } catch (Exception e) {
        LOGGER.error("An error ocurred in the worker loop", e);
      }

      if (conn != null && conn.isOpen()) {
        try {
          conn.close(5000);
        } catch (IOException e) {
          LOGGER.error("An error occurred while shutting down", e);
        }
      }

      if (!interrupted()) {
        LOGGER.info("Waiting " + nextRestartDelaySeconds + " seconds before restarting...");
        try {
          Thread.sleep(nextRestartDelaySeconds * 1000);
          nextRestartDelaySeconds = Math
              .min(MAX_RESTART_DELAY_SECONDS, nextRestartDelaySeconds * 2);
        } catch (InterruptedException e) {
          LOGGER.warn("Interrupted while sleeping", e);
          return;
        }
      } else {
        return;
      }
    }
  }

  private void connect() throws IOException {
    try {
      conn = factory.newConnection();
      channel = conn.createChannel();
    } catch (TimeoutException e) {
      throw new IOException("Timeout while connecting to RabbitMQ", e);
    }
  }
}
